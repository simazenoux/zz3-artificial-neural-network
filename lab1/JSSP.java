import java.io.*;
import java.util.*;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.constraints.IIntConstraintFactory.*;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.exception.ContradictionException;

public class JSSP {
    String id;                     // file name
    int n;                         // number of jobs
    int m;                         // number of resources  
    int dueDate;                   // aka makespan
    ArrayList<Job> jobs;           // jobs to complete
    ArrayList<Resource> resources; // resources to use
    Operation endOp;               // last operation for ALL jobs!
    Model model;

    JSSP(String fname,int dueDate) throws IOException {
	Scanner sc        = new Scanner(new File(fname));
	id                = fname;
	n                 = sc.nextInt(); // number of jobs
	m                 = sc.nextInt(); // number of resources
	jobs              = new ArrayList<Job>();
	resources         = new ArrayList<Resource>();
	this.dueDate      = dueDate;
	model             = new Model("id");
	endOp             = new Operation("endOp","nullRes",0,0,dueDate,model);
	int totalDuration = 0;
	for (int i=0;i<m;i++) resources.add(new Resource("r_"+i,model));
	for (int i=0;i<n;i++){
	    Job job = new Job("job_"+i,model);
	    for (int j=0;j<m;j++){
		Resource resource = resources.get(sc.nextInt());
		int duration      = sc.nextInt();
		totalDuration     = totalDuration + duration;
		Operation operation = new Operation("op_"+i+"_"+j,resource.id,duration,0,dueDate,model);
		resource.add(operation);
		job.add(operation);
	    }
	    job.add(endOp);
	    jobs.add(job);
	}
	model.arithm(endOp.start,"<=",totalDuration).post();
	sc.close();
    }

    public  String toString(){
	String s = "JSSP " + n + "X" + m + "\n";
	for (int i=0;i<jobs.size();i++){
	    Job job = (Job)jobs.get(i);
	    s = s + job  + "\n";
	}
	return s;
    }

    IntVar getMakeSpan(){return endOp.start;}

    Decision[] getDecisions(){
	Decision[] decisions = new Decision[((n * (n-1))/2) * m];
	for (int i=0,k=0;i<m;i++)
	    for (Decision decision : resources.get(i).decisions) 
		decisions[k++] = decision;
	return decisions;
    }
    //
    // to be used with min-slack dvo
    //

}
